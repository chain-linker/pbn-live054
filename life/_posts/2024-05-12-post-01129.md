---
layout: post
title: "모카포트 커피의 매력"
toc: true
---

  커피 애호가가 아니어도 모카포트, 유달리 비알레티의 모카포트(Moka Pot)를 보면 하나쯤은 가지고 싶은 생각이 드는데요. 저희 집에도 평범한 실버색의 비알레티 모카포트가 두 줄거리 있습니다. 반면 유럽의 제한 카페에서 색색의 다양한 사이즈의 모카포트를 전시한 걸 보고는 저도 그리도 전야 가지고 싶은 욕심이 들정도로 예뻤는데요. 인테리어 소품으로도 손색이 없지만 모카포트 커피만의 매력이 있기도 합니다.

 

 모카포트는 장 사용할 때는 긴장의 연속이죠. 저도 모카포트 운용 초기에는 가스렌지에서 뜨거운 커피가 분수처럼 하늘로 솟구친 대조적 있습니다. 어째 놀랐던지 인제 이후로 저도, 옆에서 지켜보던 엄마도 한동안 이익 모카포트를 사용하지 않고 네스프레소 커피머신에서 캡슐커피를 내려마셨는데요. 알고 보니 제목 모카포트의 스팀 부분이 박박이 잠기지 않아서였습니다. 현계 판매되는 모카포트는 정형 부분이 없어져 더이상 저처럼 커피가 폭발하는 일은 즉금 없을 거예요.
 

 

## 모카포트

 모카포트는 이탈리아 커피문화의 상징이죠. 1933년에 이탈리아의 루이지 디 폰티가 발명한 이하 알폰소 비알레티에 의해 널리 대중화된  커피 추출 기구인데요. 모카포트의 팔각형 모향은 미적인 목적뿐만 아니라 최적의  커피 추출을 위한 열 분배를 향상시키는 역할을 합니다.

 

 모카포트의 커피 추출 방식은 알고보면 간단합니다. 하단에 적정량의 물을 넣고, 중간에 커피 한스푼을 넣은 다음 끊이면 그 증기가 커피가루를 통과해 커피를 추출합니다. 따라서 드립 방식의 커피보다는 한층 농축되어 에스프레소 커피에 가까운 풍부함을 지니고 있는데요.

 

 요즘은 기존의 가스뿐만 아니라 인덕션에서도 사용할 행복 있고 전기포트처럼 전기로 끓일 삶 있는 모카포트도 있으니 필요에 따라 구하실 운 있습니다. 집에 캡슐커피머신이 있다면 원판 필요하지 않지만 모카포트는 왠지 커피마니아, 커피애호가라면 으레 가지고픈 홈카페 아이템이죠.

 

 

## 모카포트 VS 에스프레소 커피머신
 모카포트와 에스프레소 커피머신 추출 커피를 비교하면 데이터 비슷한 듯 다른점이 있는데요. 간단하게는 커피를 내리는 시간부터 다릅니다. 모카포트는 물이 끓어야 하기에 커피가 완성되기까지 허다히 5~7분 소요되는 반면, 에스프레소 커피머신은  커피 한잔을 내리는데 1분도 걸리지 않죠.

 

 아무런 방법이 더 쉬운지를 따진다면 사용하기 나름입니다. 둘다 익숙하다면 둘 일체 어려울 것이 없는 방법인데요. 물론 캡슐커피머신과 비교한다면 네스프레소나 돌체구스토, 일리커피와 같은 캡슐커피머신이 주인 쉽습니다. 캡슐만 제호 자리에 넣으면 끝이니 요령이라는 게 소요 없으니까요.

 

 다만 캠핑을 안편지 마시는 등 휴대성이 필요하다면 에스프레소 커피머신은 휴대할 생명 없으니 상의물론 모카포트죠. 도리어 드립백 등 다른 옵션이 있으니 모카포트가 캠핑장에서 1순위는 아닙니다만 모닥불에서 커피를 끓이는 낭만이 있기는 합니다.

 

 그리고, 맛을 비교하면 추출 과정에서 모카포트는 에스프레소 커피머신 만큼의 압력이 발생하지 않기 그리하여 에스프레소 커피의 강렬한 맛은 덜 합니다. 따라서, 크레마도 기대할 수 없는데요. 풍부한 풀바디의  커피, 크레마가 있는 커피를 선호한다면 에스프레소 머신만한건 없습니다.
 

 

## 모카포트 커피의 장점과 단점
 모카포트는 커피를 제출물로 추출하는 전통적인 이 방식자체가 매력입니다.  커피 애호가들은 커피를 만드는 변리 과정을 통해 커피와 우극 가까워지는 느낌을 받게 됩니다. 뿐만 아니라 커피머신에 비해서 비용이 저렴하고 가스, 전기, 심지어 모닥불 등 다양한 열, 다양한 환경에서 커피를 내릴 수 있고 크기가 귀토 견고해 장소에 구애받지 않고 장기가 사용이 가능한 장점이 있습니다.

 

 모카포트의 큰 단점은 학습이 필요하다는 점인데요. 익숙해지면 누구나 사용할 행우 있지만 완벽하게 추출하려면 연습이 필요하고 더구나 모카포트에 적합하게 분쇄된 커피가 있어여야하고 열 수준과 기운 등을 익히는 것이 중요합니다. 남달리 커피를 과도하게 추출하거나 태우지 않기 위해 열 제어가 필수라 자동화된 커피머신에 비해 주의를 요합니다. 그리고 모카포트의 크기기 작지만 막히는 것을 방지하고 한동안 사용하려면 모카포트 정말 유지관리가 필수입니다.

 

 모카포트는 이렇듯 비싼 장비없이도 에스프레소와 비슷하게 농축된 커피를 만들 길운 있다는 매력이 있습니다. 하지만 정확히는 에스프레소 커피와 차이가 있기에 에스프레소 커피를 [커피포트](https://goldfish-inhale.com/life/post-00100.html) 대체할 수는 없는데요. 개인적으로는 페이백을 받을 수 있는 네스프레소 버추오플러스가 크레마가 풍부한 커피를 맛볼수 있기에 최고지만 커피맛보다는 커피를 추출하는 그대 과정을 즐긴다면 모카포트도 매력은 있습니다.
 

 

 

 

 

 

