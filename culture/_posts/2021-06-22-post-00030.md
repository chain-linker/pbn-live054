---
layout: post
title: "2021 2분기 애니 정리"
toc: true
---


완결 애니 :1179작품 / 20226화
지난 완결 애니 :1152작품 / 19872화
이번분기 의사 완결 애니 : 27작품 / 354화
 지난분기 신작애니
 월 : 괴물사변, 논논비요리 논스톱(3기), 무직전생, 진격의 거인 파이널(4기)
화 : 벽람항로 미속전진, 이세계 피크닉
수 : 전생했더니 슬라임이엇던 건에 대하여 2기
목 : Re 제로부터 시작하는 이세계 작용 2기 2쿨, 회복술사의 재시작
금 : 5등분의 신부 2기, Dr. Stone 2기, 쓰르라미 울 적에 업, 약속의 네버랜드 2기, 유루캠 2기, 천지창조 디자인부
토 : 나만 들어갈 명 있는 숨겨진 던전, 약캐 토모자키 군, 어른의 방어구점 2기, 주술회전, 킹스레이드
일 : 반요 야사히메, 일하는 세포 2기, 일하는 세포 블랙, 호리미야
총 24작품

간단 감상
5등분의 신부 2기(A-)
 - 1기보다 나은 작화
 - 원작이 인기있었던 만치 충분히 평타는 친다
Dr. Stone 2기(A*)
 - 완벽한 완결은 아니지만 깔끔하게 끝냈다
 - 물건을 만드는게 무리 억지스러운 점이 있긴 하다
 - 초기에는 이해가 갔으나 전화기랑 동력기 이후로는 곰곰이 뇌를 비우고 봤다
 - 소년물 성격으로 앞으로도 기대되는 작품
Re 제로부터 시작하는 이세계 한생 2기 2쿨(B+)
 - 성역편 끝
 - 오랜 공백 이후에 보는지라 세계관과 주조연들의 발자취가 가물가물해서 집중은 심도 안됐다
 - 거기에 신캐는 구속 나오는데 얘가 단역인지 중요인물인지 감이 잡히지 않는다
 - 결국 원작을 본 담에 다시봐야 당각 된 재미를 느낄 행복 있을 것 같다
괴물사변(A*)
 - 차후 2기가 기대되는 작품
 - 아직까진 어렵지 않은 내용에 회소 및 전투씬 상급
 - 아직까진 주인공이 강한데 무덤덤해서 긴장감이 들진 않는다
나만 들어갈 요체 있는 숨겨진 던전(B-)
 - 전형적인 여캐팔이 판타지물
 - 앞부분은 여캐가 상당히 괜찮았는데 뒤로 가면서 작화가 차차로 심각해진다
 - LP가 없으면 죽으나 작품의 분위기상 숫자놀음이 가능해서 위기감이 들지 않는다
 - 캐릭터는 괜찮은데 참..
논논비요리 논스톱(3기)(A*)
 - 이번분기 힐링물
 - 바지런히 괜찮다
 - 개인적으로는 2화
무직전생(A*)
 - 작화에 힘을 넣은 작품, 보는 재미는 확실하다
 - 주인공의 성격, 초반 직책 관계에서 호불호가 있을 생령 있다
 - 참으로 2기 공기 작품
반요 야사히메(B*)
 - 완결난 작품의 생명줄을 억지로 늘리는 작품
 - 여러 요괴가 재탕해서 나오는데 이누야사때의 기억이 나는 그럼에도 불구하고 원체 재탕이라 식상한 대응 있다
 - 처음부터 끝까지 파워밸런스에서 의문감이 든다
 - 말이 반요지만 운동신경은 인간쪽인데 서토 합을 나누다가 기술 한번쓰면 끝나는 패턴의 무한반복
벽람항로 미속전진(A+)
 - 10분
 - 이전작은 되도않는 설 만든다고 했다가 말아먹고 이번에 진정한 곧바로 작품으로 새로이 태어났다
 - 식 4컷만화의 애니화로 평범한 일상물
 - 짧은 시간안에 캐릭터가 많으면 햇갈릴 운명 있으나 제각각 맡은 파트가 있어서 어지럽지는 않다
쓰르라미 울 적에 업(A*)
 - 이득 작품은 쓰르라미 리메이크입니다 라고 용기사가 말을 했다가 거하게 통수를 맞고 많은 욕을 먹었다
 - 기본적인 진술 라인을 따라가는데에는 문제가 없으나 세세한 설정은 컷이나 짧게 지나가서 이해하기 힘들다
 - 미요의 런과 우는거에서는 ??? 부근 찍힐 요행 밖에 없고 누워있는 오빠는 쭉 불치병이구나 라고 생각할 수 경로 있다
 - 왜그런지 궁금하면 원작 애니를 보거나 원작 게임이나 코믹스를 보라고 할 성명 밖에 없다
 - 총을 들이밀고 웃는 네년 나중 씬은 나중에 입몰 에서
약속의 네버랜드 2기(B-)
 - 1기와는 퍽 다른 장르
 - 1기가 탈출 및 그대 과정에서의 심리전과 액션이였으면 2기는 생존과 획일화 게다가 복귀?
 - 마지막에 호위호 돌아갔는지가 이해가 안간다
 - 세계관을 확장하기 위한 커밍아웃이 나오는데 도로 해결이 안된체 끝났다
약캐 토모자키 군(A*)
 - 연애물
 - 찐따 주인공을 여주가 갱생하는 흔한 내용
 - 소득 보기 캐릭터와 화 및 성우로 밀고가는데 형씨 점은 합격
어른의 방어구점 2기(B*)
 - 3분
 - 3분치고는 캐릭터가 엄청 움직여서 봤으나 특별한 느낌은 없다
유루캠 2기(A*)
 - 캠핑 힐링물
 - 잔잔하게 커피나 차를 옆에 놓고 마시면서 보기 좋다
이세계 피크닉(B+)
 - 보편적으로 말하는 이세계 보다는 뒷(우라)세계 에 가깝다
 - 액선이 화려한것도 아니고 추리나 모험이 기본 도 아니고 형식 파악이 힘들었다
일하는 세포 2기(A+)
 - 몸 내용물 세포의 의인화 2기 희망편
 - 보면 낌새 몸에 대해 다시한번 생각하게 된다
 - 많이 많은 부분이 유산균에 대한 이야기, 우유를 적잖이 먹읍시다
일하는 세포 블랙(A+)
 - 절망편
 - 흡연 음주 밤샘 운동부족은 위험합니다
 - 모공세포에서 여드름vs탈모 / 유산균vs각종 약물
 - 두 작품을 1화씩 돌아가며 봐도 되고 하나만 변함없이 봐도 상관은 없다
전생했더니 슬라임이었던 건에 대하여 2기(A-)
 - 1쿨분량이라서 그런지 에피소드 단특 및 스토리진행 약간
 - 버프와 디버프로 무장해서 상대적 셀 줄 알았던 대조적 나중엔 너무 허무하게 갔다
 - 마지막화 끊기때문에 다음기 염원 작품
주술회전(A-)
 - 전투씬 액션은 최상급
 - 세계관과 앞부분 흥미유도는 좋다
 - 중반 부터는 내용은 어찌되든 상관없고 전투씬만 봤다
 - 구담 라인을 보면서도 까먹어서 2기는 기대가 되지 않는다
진격의 거인 파이널(4기)(A-)
 - 인생철학 떡밥 풀이 타임
 - 캐릭터들이 늠름해져서 거인을 제외하면 신작이라고 봐도 될 것 같다
 - 무심히 순수하게 재미로만 보는 사람이면 2기까지 보고 마는게 제일 좋다
천지창조 디자인부(B+)
 - 각반 희귀한 동물은 되는데 왜 뿔달린 말이 없냐고 한탄하는 애니
 - 나름 교육적
 - 만담은 재미가 없다
킹스레이드(B*)
 - 모바일 오락 애니화
 - 악이 존재하고 그를 잡아가는 일반적인 모험물 스토리를 따라가고 있다
 - 내용이나 작화는 무난
 - 엔딩을 독점한 다크엘프가 한계 [무료애니](https://quarrel-sleepy.com/culture/post-00002.html) 건 할 줄 알았으나 허무했다
호리미야(A*)
 - 연애물
 - 앞부분 주연들의 연애이야기는 재미있으나 빠르게 결론이 나서 그쪽 이후는 조금 심심하다
회복술사의 재시작(A-)
 - 19금
 - 자극이 필요하다고 느끼면 추천
 - 담론 라인이 괜찮고 주인공의 심리가 이해가 간다

지난분기 신작
디지몬 어드벤처
거미입니다만, 문제라도
백 애로우
총 3작품

이번분기 신작
월 :
화 :
수 :
목 :
금 :
토 :
일 :
총 *작품

감상한 완결작

Fate Stay Night HF 극장판(A+)
 - 무려 극장판이 3부작
 - 많은 시간이 들어간 만큼 삭제되거나 간단하게 된 부분은 있어도 넣을 씬은 다 넣었다
 - 이펙트가 화려하고 보는 재미는 최상급
고블린 슬레이어 극장판(A*)
 - 극장판이라기 보단 tv판의 연장선 느낌
 - 잔인한장면이 여과없이 나온다
 - 극장판답게 작화는 상급
리틀 버스터즈 극장판 쿠드 와푸타(A+)
 - 깔끔한 기승전결
 - 달달한 연애이야기로 의도 편하게 전례 좋다
좀비 랜드 사가(A-)
 - 이번에 2기가 나와서 급하게 몰아 본 작품
 - 아이돌애니는 거르는 편이지만 전체적으로 보니까 꽤 괜찮다
 - 말 못하는 좀비는 백날 말을 하게 될지가 궁금
